#!/bin/bash
#
# Runs CirrusSearch integration tests from within the mwdd environment.

set -e

echo '## Starting run-integration.sh'
SCRIPT_DIR="$(realpath "$(dirname "$0")")"
cd "$SCRIPT_DIR"
. ./env

# Allows specifying a single feature file
CIRRUS_FEATURES="${CIRRUS_FEATURES:-tests/integration/features/*.feature}"


if [ "$($MW docker env get PORT)" != "8080" ]; then
    echo "## Env not running on expected port"
    echo 'Use `mw docker env set PORT 8080` to configure the expected port'
    echo 'Typically this happens because something else had port 8080 when'
    echo 'creating the environment.'
    exit 1
fi

# Ensure we get a fresh fresh
$MW docker docker-compose stop mediawiki-fresh
$MW docker docker-compose rm -- --force mediawiki-fresh

echo '## Running integration test suite...'
sleep 2
$MW docker mediawiki fresh bash -- -c "cd extensions/CirrusSearch && npm install && \
    WEBDRIVER_IO_CONFIG_FILE=tests/integration/config/wdio.conf.mwdd.js \
    MEDIAWIKI_CIRRUSTEST_BOT_PASSWORD='Cindy@$ADMIN_BOT_PASSWORD' \
    MEDIAWIKI_COMMONS_BOT_PASSWORD='Cindy@$ADMIN_BOT_PASSWORD' \
    MEDIAWIKI_RU_BOT_PASSWORD='Cindy@$ADMIN_BOT_PASSWORD' \
    MEDIAWIKI_BOT_PASSWORD='Cindy@$ADMIN_BOT_PASSWORD' \
    MEDIAWIKI_PASSWORD=mwddpassword \
    CIRRUS_TAGS='not @expect_failure' \
    CIRRUS_FEATURES='${CIRRUS_FEATURES}' \
    npm run selenium"

echo '## Completed run-integration.sh'
