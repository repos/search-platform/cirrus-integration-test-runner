<?php

wfLoadExtension( 'TimedMediaHandler' );

$wgFFmpegLocation = "/usr/bin/ffmpeg";
$wgTmhFluidsynthLocation = "/usr/bin/fluidsynth";
$wgTmhSoundfontLocation = "/usr/share/sounds/sf2/FluidR3_GM.sf2";
$wgWaitTimeForTranscodeReset = 1;
