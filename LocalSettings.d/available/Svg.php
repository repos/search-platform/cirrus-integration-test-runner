<?php

$wgAllowTitlesInSVG = true;
$wgSVGConverter = "rsvg";
$wgSVGConverters["rsvg"] = '$path/rsvg-convert -w $width -h $height $input -o $output';
