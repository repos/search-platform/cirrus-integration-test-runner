<?php

wfLoadExtension('Elastica');
wfLoadExtension('CirrusSearch');

// Should always exist, but not merged yet.
if ( file_exists("$IP/extensions/CirrusSearch/tests/jenkins/IntegrationTesting.php" ) ) {
    require_once "$IP/extensions/CirrusSearch/tests/jenkins/IntegrationTesting.php";
}
$wgSearchType = 'CirrusSearch';
$wgCirrusSearchClusters = [ 'default' => $wgCirrusSearchServers ];
$wgCirrusSearchExtraIndexes[NS_FILE] = ['commonswiki_file'];
$wgCirrusSearchDevelOptions['allow_nuke'] = true;

unset( $wgCirrusSearchServers );

