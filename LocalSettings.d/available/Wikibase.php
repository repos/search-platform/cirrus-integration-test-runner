<?php

wfLoadExtension( 'WikibaseRepository', "$IP/extensions/Wikibase/extension-repo.json" );
wfLoadExtension( 'WikibaseClient', "$IP/extensions/Wikibase/extension-client.json" );
wfLoadExtension( 'WikibaseCirrusSearch' );

call_user_func( function() {
	$baseNs = 120;

	// Define these constants immediately so they can be used in LocalSettings.php
	define( 'WB_NS_ITEM', $baseNs );
	define( 'WB_NS_ITEM_TALK', $baseNs + 1 );
	define( 'WB_NS_PROPERTY', $baseNs + 2 );
	define( 'WB_NS_PROPERTY_TALK', $baseNs + 3 );
} );

// Tell Wikibase to register these namespaces (later)
global $wgWBRepoSettings;
$wgWBRepoSettings['defaultEntityNamespaces'] = true;

// Cirrus stuff
$wgWBCSUseCirrus = true;
// Mimic prod config from T328276
$wgCirrusSearchDeduplicateAnalysis = true;
$wgWBCSDefaultFulltextRescoreProfile = 'wikibase_config_phrase';

$wgWBCSFulltextSearchProfiles = [
    'wikibase_config_fulltext_query' => [
        'builder_factory' => [
            '\Wikibase\Search\Elastic\EntityFullTextQueryBuilder',
            'newFromGlobals'
        ],
        'settings' => [],
    ],
];

$wgWBCSRescoreProfiles['wikibase_config_phrase'] = [
    'i18n_msg' => 'wikibase-rescore-profile-fulltext',
    'supported_namespaces' => 'all',
    'rescore' => [
        // phrase rescore
        [
            'window' => 512,
            'window_size_override' => 'CirrusSearchPhraseRescoreWindowSize',
            'rescore_query_weight' => 10,
            'rescore_query_weight_override' => 'CirrusSearchPhraseRescoreBoost',
            'query_weight' => 1.0,
            'type' => 'phrase',
            // defaults: 'score_mode' => 'total'
        ],
        [
            'window' => 8192,
            'window_size_override' => 'EntitySearchRescoreWindowSize',
            'query_weight' => 1.0,
            'rescore_query_weight' => 2.0,
            'score_mode' => 'total',
            'type' => 'function_score',
            'function_chain' => 'entity_weight_boost'
        ],
    ],
];

$wgWBCSUseStemming = [
    'ar' => [ 'index' => true, 'query' => true ],
    'bg' => [ 'index' => true, 'query' => true ],
    'ca' => [ 'index' => true, 'query' => true ],
    'ckb' => [ 'index' => true, 'query' => true ],
    'cs' => [ 'index' => true, 'query' => true ],
    'da' => [ 'index' => true, 'query' => true ],
    'de' => [ 'index' => true, 'query' => true ],
    'el' => [ 'index' => true, 'query' => true ],
    'en' => [ 'index' => true, 'query' => true ],
    'en-ca' => [ 'index' => true, 'query' => true ],
    'en-gb' => [ 'index' => true, 'query' => true ],
    'es' => [ 'index' => true, 'query' => true ],
    'eu' => [ 'index' => true, 'query' => true ],
    'fa' => [ 'index' => true, 'query' => true ],
    'fi' => [ 'index' => true, 'query' => true ],
    'fr' => [ 'index' => true, 'query' => true ],
    'ga' => [ 'index' => true, 'query' => true ],
    'gl' => [ 'index' => true, 'query' => true ],
    'he' => [ 'index' => true, 'query' => true ],
    'hi' => [ 'index' => true, 'query' => true ],
    'hu' => [ 'index' => true, 'query' => true ],
    'hy' => [ 'index' => true, 'query' => true ],
    'id' => [ 'index' => true, 'query' => true ],
    'it' => [ 'index' => true, 'query' => true ],
    'ja' => [ 'index' => true, 'query' => true ],
    'ko' => [ 'index' => true, 'query' => true ],
    'lt' => [ 'index' => true, 'query' => true ],
    'lv' => [ 'index' => true, 'query' => true ],
    'nb' => [ 'index' => true, 'query' => true ],
    'nl' => [ 'index' => true, 'query' => true ],
    'nn' => [ 'index' => true, 'query' => true ],
    'pl' => [ 'index' => true, 'query' => true ],
    'pt' => [ 'index' => true, 'query' => true ],
    'pt-br' => [ 'index' => true, 'query' => true ],
    'ro' => [ 'index' => true, 'query' => true ],
    'ru' => [ 'index' => true, 'query' => true ],
    'simple' => [ 'index' => true, 'query' => true ],
    'sv' => [ 'index' => true, 'query' => true ],
    'th' => [ 'index' => true, 'query' => true ],
    'tr' => [ 'index' => true, 'query' => true ],
    'uk' => [ 'index' => true, 'query' => true ],
    'zh' => [ 'index' => true, 'query' => true ],
];
