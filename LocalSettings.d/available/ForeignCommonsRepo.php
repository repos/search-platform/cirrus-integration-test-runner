<?php

$wgUseInstantCommons = false;
$wgForeignFileRepos[] = [
        'class'            => 'ForeignDBViaLBRepo',
        'name'             => 'shared',
        'directory'        => '/srv/commonsimages',
        'url'              => '//commons.wiki.local.wmftest.net/commonsimages',
        'hashLevels'       => 2,
        'thumbScriptUrl'   => false,
        'transformVia404'  => true,
        'hasSharedCache'   => true,
        'descBaseUrl'      => '//commons.wiki.local.wmftest.net/wiki/File:',
        'scriptDirUrl'     => '//commons.wiki.local.wmftest.net/w',
        'fetchDescription' => true,
        'wiki'             => 'commonswiki',
        'initialCapital'   => true,
        'abbrvThreshold'   => 160,
];
