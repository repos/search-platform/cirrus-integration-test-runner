<?php

$wgCirrusSearchNamespaceMappings[NS_FILE] = 'file';
$wgCirrusSearchReplicaCount['file'] = 1;
$wgCirrusSearchShardCount['file'] = 1;
// This config must be installed with a later priority than
// the main CirrusSearch configuration.
unset( $wgCirrusSearchExtraIndexes[ NS_FILE ] );
