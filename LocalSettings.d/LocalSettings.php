<?php

wfLoadSkin( 'Vector' );

if ( substr( $wgDBname, 0, 2 ) === 'ru' ) {
    $wgLanguageCode = 'ru';
}

foreach ( glob( "$IP/LocalSettings.d/configured/{$dockerDb}/*.php" ) as $path ) {
    require_once $path;
}
