#!/usr/bin/env bash

SCRIPT_DIR="$(dirname "$(realpath "$0")")"
cd "$SCRIPT_DIR"
. ./env

if ! git review --version &> /dev/null; then
	echo "Git review must be installed, run apt install git-review as root?"
	exit 1
fi
git-update() {
    echo '## Running a git-update'
    MEDIAWIKI_SRC="$($MW docker env get MEDIAWIKI_VOLUMES_CODE)"
    find "$MEDIAWIKI_SRC" -type d -name .git | while read -r gitdir; do
        (cd "$(dirname "$gitdir")" && git pull --ff-only)
    done
}

while true; do
    (( number = RANDOM % 40 ))
    if [ "$number" -eq 0 ]; then
        git-update
    fi
    echo '## Running barrybot.py'
    python3 barrybot.py \
        --verbose 1 \
        --project mediawiki/extensions/CirrusSearch \
        --core mediawiki \
        --test mediawiki/extensions/CirrusSearch \
        --branch master \
        --user Cindy-the-browser-test-bot \
        --successmsg 'Cindy says good job \o/. Keep it up.' \
        --errormsg 'I ran tests for your patch and there were some errors: %s'
    echo '## Pausing before next run'
    sleep 600
done
