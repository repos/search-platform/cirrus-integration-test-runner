#!/bin/bash

./mw docker mediawiki exec -- /usr/bin/env CIRRUS_REBUILD_FIXTURES=${CIRRUS_REBUILD_FIXTURES:-yes} PHPUNIT_WIKI=${PHPUNIT_WIKI:-cirrustestwiki} php tests/phpunit/phpunit.php "$@"

