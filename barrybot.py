#!/usr/bin/env python

'''
Copyright [2013] [Jon Robson]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and
limitations under the License.
'''
import argparse
import glob
from itertools import islice
import json
import junitparser
import os
import re
import subprocess
import sys
import urllib.request
from urllib.parse import quote_plus
from xml.etree import ElementTree
from lxml.etree import XMLSyntaxError

def run_shell_command(args, verbose = False):
    """
    run_shell_command(['echo', 'hi']) runs 'echo hi'
    """
    cmd = " ".join(args)
    if verbose:
        print(f"Running `{cmd}`")
    sys.stdout.flush()
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    output = [];
    for line_bytes in process.stdout:
        line = line_bytes.decode('utf8')
        output.append(line)
        if verbose:
            print(line, end='', flush=True)
    process.wait()
    return "".join(output), process.returncode > 0

def update_code_to_master(paths, verbose = False):
    print("Updating to latest code...")
    for path in paths:
        run_shell_command([
            'cd', path, '&&',
            # Stuff might be dirty
            'git', 'stash', '&&',
            'git', 'checkout', 'master', '&&',
            # Update code
            'git', 'pull', 'origin', 'master'
        ], verbose=verbose)

def get_pending_changes(project, user, verbose, branch):
    query = ' '.join([
        f'project:{project}',
        f'branch:{branch}',
        'status:open',
        f'NOT label:Verified=1,user={user}',
        f'NOT label:Verified=-1,user={user}',
        'NOT age:2w',
    ])
    url = f"https://gerrit.wikimedia.org/r/changes/?q={quote_plus(query)}&O=1"
    print(f"Request {url}")
    req = urllib.request.Request(url)
    req.add_header('Accept', 'application/json')
    with urllib.request.urlopen(req) as f:
        content = f.read()
        # 5: is because we need to ignore gerrits anti-hijacking prefix
        return json.loads(content[5:])

def checkout_commit(path, changeid, verbose = False):
    print(f"Preparing to test change {changeid}...")
    output, error = run_shell_command([
        "cd", path, "&&",
        # might be in a dirty state
        'git', 'stash', '&&',
        'git', 'checkout', 'master', '&&',
        "git", "review", "-d", changeid
     ], verbose=verbose)
    # get the latest commit
    output, error = run_shell_command([
        "cd", path, "&&",
        "git", "rev-parse", "HEAD"
    ], verbose=verbose)
    commit = output.strip()
    return commit

def checkout_dependencies(mediawikipath, pathtotest, verbose = False):
    output, error = run_shell_command([
        "cd", pathtotest, "&&",
        "git", "log", "-1", "|",
        "grep", "'^    Depends-On:'", "|",
        "cut", "-d", ':', '-f', '2-'
    ], verbose=verbose)
    for line in output.strip(' ').split("\n"):
        commit_id = line.strip()
        if commit_id == '':
            continue
        # bold assumption the commit id is a core id
        run_shell_command([
            "cd", mediawikipath, "&&",
            "git", "review", "-d", commit_id
        ], verbose=verbose)


def parse_junit(log_path):
    failures = set()
    bad_files = 0
    for fname in glob.glob(f'{log_path}/*.xml'):
        try:
            print(f"Parsing junit from {fname}")
            suites = junitparser.JUnitXml.fromfile(fname)
        except (ElementTree.ParseError, XMLSyntaxError):
            bad_files += 1
        else:
            failed_cases = [(suite, case) for suite in suites for case in suite if case.result]
            if failed_cases:
                print("Failed cases:")
                for suite, case in failed_cases:
                    print(f"\n***\n\n{case}\n")
                    failures.add(case.name)
    if bad_files:
        failures.add(f'Could not parse {bad_files} junit log(s)')
    output = f'Encountered {len(failures)} failures.'
    if len(failures) > 10:
        output += ' Results truncated.'
    output += '\n' + '\n'.join(islice(failures, 10))
    if len(failures) > 10:
        output += '\n...'
    return output


def clean_junit_logs(log_path):
    for fname in glob.glob(f'{log_path}/*.xml'):
        try:
            os.unlink(fname)
        except:
            raise RuntimeError(f'Failed to unlink `{fname}` ?!?')

def run_integration_tests(path, verbose = False, full_log = None):
    print('Running integration tests...')
    output, error = run_shell_command([
        './create-env.sh', '&&',
        './run-integration.sh'
    ], verbose=verbose)

    if full_log:
        with open(full_log, 'a') as f:
            f.write(output)

    return not error

def do_review(pathtotest, commit, is_good, msg, action, verbose = False, user = None):
    print("Posting to Gerrit...")
    args = [
        'cd', pathtotest, '&&',
        'ssh', '-p 29418',
        'cindythebrowsertestbot@gerrit.wikimedia.org',
        'gerrit', 'review',
        '--' + action,
        '+1' if is_good else '-1',
    ]
    if msg:
        # TODO: why is message passed "'wrapped in both kinds of quotes'"
        args.extend(['--message', "\"'" + msg.replace('"', '').replace("'", '').replace('$', '\$') + "'\"" ])
    args.append(commit)
    # Turn on when you trust it.
    output, error = run_shell_command(args, verbose=verbose)

def get_parser_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--project', help='Name of project e.g. mediawiki/extension/Gather', type=str, required=True)
    parser.add_argument('--core', help='Absolute path to core e.g /git/core', type=str, required=True)
    parser.add_argument('--branch', help='Branch to look for patches to', default='master', type=str)
    parser.add_argument('--test', help='Absolute path that corresponds to project to be tested. e.g /git/core', type=str, required=True)
    parser.add_argument('--dependencies', help='Absolute path to a dependency e.g /git/core/extensions/MobileFrontend', type=str, nargs='+')
    parser.add_argument('--noupdates', help='Will prevent checking out different versions of repositories other than test', type=bool)
    parser.add_argument('--review', help='This will post actual reviews to Gerrit. Only use when you are sure Barry is working.', type=bool)
    parser.add_argument('--verify', help='This will post actual reviews to Gerrit. Only use when you are sure Barry is working.', type=bool)
    parser.add_argument('--verbose', help='Advanced debugging.', type=bool)
    parser.add_argument('--user', help='The username of the bot which will do the review.', type=str)
    parser.add_argument('--successmsg', help='Defines the message to show for successful commits', type=str, default='I ran browser tests for your patch and everything looks good. Merge with confidence\!')
    parser.add_argument('--errormsg', help='Defines the message to show for bad commits', type=str, default='I ran browser tests for your patch and there were some errors you might want to look into:\n%s')
    return parser

def get_username(args):
    if args.user:
        user = args.user
    else:
        user, code = run_shell_command(['git config --global user.name'])
        user = user.strip()
    return user

def get_paths(args):
    if args.dependencies:
        dependencies = args.dependencies
    else:
        dependencies = []
    paths = [args.core, args.test]
    paths.extend(dependencies)
    return paths

def main(args):
    verbose = args.verbose

    paths = get_paths(args)
    print("Searching for patches to review...")
    user = get_username(args)
    changes = get_pending_changes(args.project, user, verbose, args.branch)
    if len(changes) == 0:
        print("No changes.")

    for change in changes:
        print(f"Testing {change['subject']}")
        test_change(change["_number"], args)


def test_change(change_id, args):
    action = 'code-review' if args.review else 'verified'
    paths = get_paths(args)
    user = get_username(args)
    if not args.noupdates:
        update_code_to_master(paths, args.verbose)
    commit = checkout_commit(args.test, str(change_id), args.verbose)
    if not args.noupdates:
        checkout_dependencies(args.core, args.test, args.verbose)

    full_log = f'/tmp/cindy_run.{change_id}.log'
    junit_log = f'{args.test}/tests/integration/log/'
    # Source directories are retained between runs, so we need to clear out
    # the junit logs from previous runs if they exist.
    clean_junit_logs(junit_log)
    is_good = run_integration_tests(args.test, args.verbose, full_log)
    if is_good:
        review_msg = args.successmsg
    else:
        review_msg = args.errormsg % (parse_junit(junit_log),)

    if args.verbose:
        print(review_msg)

    if action:
        print(f'Reviewing commit {commit} with (is good = {is_good})..')
        do_review(args.test, commit, is_good, review_msg, action, args.verbose, user)

if __name__ == '__main__':
    parser = get_parser_arguments()
    args = parser.parse_args()
    main(args)

