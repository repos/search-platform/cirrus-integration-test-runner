# CirrusSearch Integration Testing Runner

This repository consists of two parts that work together. Part one is
assembling a multi-wiki mediawiki environment using mwcli. The second part is
querying gerrit for patches to review, checking out the relevant code, and
running the integration tests against that code. Together this implements the
Cindy-the-browser-test-bot automation that auto-votes on CirrusSearch patches
at gerrit.wikimedia.org.

## Initial Setup in wmcloud

Create a new instance:

    Image: debian-11.0-bullseye
    Flavor: g3.cores4.ram8.disk20


Install required packages:
```
sudo apt update
sudo apt-get -y install docker.io docker-compose python3-junitparser git-review
```

Create a user, give them the rights to use docker, and make them easy to sudo to:
```
sudo useradd integration-bot --create-home --groups docker --shell "$(which bash)"
echo '%wikidev ALL = (integration-bot) NOPASSWD: ALL' | sudo tee /etc/sudoers.d/integration-bot >/dev/null
sudo -u integration-bot -i bash
```

Clone this repo somewhere:
```
git clone git@gitlab.wikimedia.org:repos/search-platform/cirrus-integration-test-runner.git
cd cirrus-integration-test-runner
```

Retrieve a recent version of the mwcli binary into the integration runner directory:
```
curl -o mw https://gitlab.wikimedia.org/repos/releng/cli/-/package_files/1178/download
chmod +x ./mw
```

Run the environment setup first time through to get it initialized and verify it works:
```
./create-env.sh
```

### CI bot setup

If this will be using run-cindy.sh / barrybot.py and reviewing gerrit PRs it will
need further configuration.

Create an ssh key:
```
sudo -u integration-bot ssh-keygen
```

Log into the bot account and tell gerrit about it at
https://gerrit.wikimedia.org/r/settings/#SSHKeys


## Scripts

### run-cindy.sh

Main entrypoint for running the gerrit review workflow. Invokes barrybot.py
on a regular basis with the desired configuration. It semi-regularly pulls new
code to all git repos it knows about.

### barrybot.py

Queries gerrit for patches to review, checkes them out, and invokes
create-env.sh along with run-integration.sh to run each patch in a new
environment. Votes on gerrit with the results of integration testing.

### create-env.sh

Destroys any running environment and creates it again. Retains wiki configuration,
source code, and vendor directories between runs.

### run-integration.sh

Runs the integration testing suite. Individual feature files can be executed by setting
the `CIRRUS_FEATURES` environment var:
```
CIRRUS_FEATURES=tests/integration/features/full_text_browser.feature ./run-integration
```

### first-run.sh

Invoked by create-env.sh, no need to use directly. Does various tasks that
only need to occur on initial configuration of the environment.  Includes mwcli
configuration, fetching source code, and preparing the multi-wiki configuration.
