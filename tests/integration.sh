#!/usr/bin/env bash
#
# This test installs the environment and runs the CirrusSearch integration test suite.

set -e # Fail on errors

# Aquire the mwcli binary. Is there a better cross-platform way to do this?
wget -O ./mw https://gitlab.wikimedia.org/repos/releng/cli/-/package_files/1178/download
chmod +x ./mw

function finish {
    ./mw docker destroy --no-interaction
    ./mw docker env clear --no-interaction
}
trap finish EXIT

./create-env.sh
./run-integration.sh
