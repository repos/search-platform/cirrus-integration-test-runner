#!/bin/bash

set -e

echo '## Start create-env.sh'

SCRIPT_DIR="$(dirname "$(realpath "$0")")"
cd "$SCRIPT_DIR"
. ./env

# Cleanup any messes
$MW docker destroy
if [ "$DESTROY_MW_SRC" = "1" ]; then
    echo '## Deleting all mwcli configuration'
    rm -rf "$($MW docker mediawiki where)"
    rm .first_run
fi
if [ "$DESTROY_MW_CLI" = "1" ]; then
    echo '## Deleting mediawiki source tree'
    rm -rf "$(dirname "$($MW config where)")"
fi

if [ ! -f .first_run ]; then
    ./first-run.sh
    touch .first_run
fi

echo '## Ensure we are using the latest settings'
# While symlinking would be nicer, the containers only mount the mediawiki
# volume and don't have access to this repo's contents directly. This might be
# annoying and mysterious when custom settings get overwritten, but with it
# running constantly hopefully the users put their config in new unmanaged
# files.
cp -rf "${SCRIPT_DIR}"/LocalSettings.d/available/* "$($MW docker env get MEDIAWIKI_VOLUMES_CODE)"/LocalSettings.d/available/

echo '## Set image vars'

$MW docker env set ELASTICSEARCH_IMAGE "${ELASTICSEARCH_IMAGE}"
$MW docker env set MEDIAWIKI_FRESH_IMAGE "${MEDIAWIKI_FRESH_IMAGE}"

echo '## Create containers'
# Create instances
echo "   Using MEDIAWIKI_IMAGE $MEDIAWIKI_IMAGE"
export MEDIAWIKI_IMAGE
$MW docker mediawiki create
$MW docker redis create
test "$SKIP_JOBRUNNER" != 1 && $MW docker mediawiki jobrunner create
$MW docker mysql create
$MW docker elasticsearch create
$MW docker shellbox media create
# TODO: necessary?
$MW docker eventlogging create

echo '## Install additional packages'

# ghostscript is needed on the mw instance for pdf uploads
$MW docker mediawiki exec -u root -- apt-get update
$MW docker mediawiki exec -u root -- apt-get -y install ghostscript

# Composer
$MW docker mediawiki exec composer update

echo '## Create wikis'
# Create matching wikis
# In theory this could all happen in parallel, but seems error prone.
for wiki in $WIKIS; do
    echo "## Create wiki $wiki"
    $MW docker mediawiki install --dbtype=mysql --dbname="$wiki"
    # While we set the dbname above, that wasn't enough to load our per-wiki
    # configuration during initial setup, and we are thus missing extension
    # tables.  This will create those tables and also ensure cirrus indices are
    # built.
    $MW docker mediawiki exec -- php maintenance/run.php update --wiki "$wiki" --quick

    # Setup various per-wiki things
    $MW docker mediawiki jobrunner add-site "$wiki"
    $MW docker mediawiki exec -- php maintenance/run.php createBotPassword \
        --wiki "$wiki" \
        Admin "$ADMIN_BOT_PASSWORD" \
        --grants basic,highvolume,editpage,editprotected,editmycssjs,editmyoptions,editinterface,editsiteconfig,createeditmovepage,uploadfile,uploadeditmovefile,patrol,rollback,blockusers,viewdeleted,viewrestrictedlogs,delete,protect,viewmywatchlist,editmywatchlist,sendemail,createaccount,privateinfo \
        --appid Cindy
done
echo '## create-env.sh Complete'
