#!/bin/bash

set -e

echo '## Starting first-run.sh'

SCRIPT_DIR="$(dirname "$(realpath "$0")")"
cd "$SCRIPT_DIR"
. ./env

# For whatever reason the config doesn't initialize for a first run unless we
# run a `mw docker <foo>` command first but allow for a failure because if
# it's actually the first time it runs it'll fail because
# MEDIAWIKI_VOLUMES_CODE is not net
$MW docker stop || true

echo '## Set image vars'

$MW docker env set ELASTICSEARCH_IMAGE "${ELASTICSEARCH_IMAGE}"
$MW docker env set MEDIAWIKI_FRESH_IMAGE "${MEDIAWIKI_FRESH_IMAGE}"

# The integration test configuration expects port 8080
if ! $MW docker env has PORT >/dev/null; then
    $MW docker env set PORT 8080
fi

echo '## Set MW volumes code'
if ! $MW docker env has MEDIAWIKI_VOLUMES_CODE >/dev/null; then
    # For a general development environment the default location is sane,
    # but for a CI bot it seems more convenient to keep the src as a subdir.
    $MW docker env set MEDIAWIKI_VOLUMES_CODE "$SCRIPT_DIR/mediawiki"
fi

MEDIAWIKI_SRC="$($MW docker env get MEDIAWIKI_VOLUMES_CODE)"
echo "## Expecting mediawiki source at : $MEDIAWIKI_SRC"

if [ ! -d "${MEDIAWIKI_SRC}" ]; then
    echo '## Installing mediawiki, extension and skin repos'
    $MW docker mediawiki get-code --gerrit-interaction-type http --core --skin Vector
    for extension in $EXTENSIONS; do
        $MW docker mediawiki get-code --gerrit-interaction-type http --extension "$extension"
    done
fi

echo '## Updating composer dependencies'
# Composer
cp "${MEDIAWIKI_SRC}/composer.local.json-sample" "${MEDIAWIKI_SRC}/composer.local.json"

# per-wiki config via custom LocalSettings.d
if [ ! -d "${MEDIAWIKI_SRC}/LocalSettings.d" ]; then
    echo "## Setting up LocalSettings.d configuration"
    target="${MEDIAWIKI_SRC}/LocalSettings.d"
    cp -rf LocalSettings.d "$target"
    if ! [ -f "${MEDIAWIKI_SRC}/LocalSettings.php" ]; then
        # Setup mwdd shim (would be auto-magic on install) along with loading
        # our LocalSettings.d.

        cat >"${MEDIAWIKI_SRC}/LocalSettings.php" <<EOD
<?php
require_once "\$IP/includes/PlatformSettings.php";
require_once "/mwdd/MwddSettings.php";
require_once "\$IP/LocalSettings.d/LocalSettings.php";
EOD
    fi

    for wiki in $WIKIS; do
        mkdir "$target/configured/$wiki"
    done
    for extension in $ALL_WIKI_EXTENSIONS; do
        "$target"/config-enable '*' "${extension}.php"
    done
    "$target"/config-enable '*' ForeignCommonsRepo.php
    "$target"/config-disable commonswiki ForeignCommonsRepo.php
    "$target"/config-enable commonswiki CirrusSearch-commonswiki.php 30
    "$target"/config-enable wikidatawiki Wikibase.php
fi

echo '## Completed first-run.sh'
